﻿using UnityEngine;
using System.Collections;

public class ShotGun : Pistola {

    [SerializeField]
    private int balas_shotGun;

    void OnEnable()
    {
        recargar.SetActive(false);

        GameController.municion_total_shotGun = disparosTotales;

        GameController.municion = disparosTotales - disparos;
    }

    public override void Start()
    {
        disparos = 0;
        recargar.SetActive(false);
        disparosTotales = balas_shotGun ;
    }



    public override void Shoot()
    {
        for (int i = 0; i < 2; i++)
        {
            Rigidbody bala = Balas_Pool.Balas_pool.ObtenerBalas();
            bala.transform.position = transform.position;
            bala.velocity = (transform.forward * fuerza);
        }
   }

}
