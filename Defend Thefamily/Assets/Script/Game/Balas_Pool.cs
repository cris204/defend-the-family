﻿using UnityEngine;
using System.Collections.Generic; //casi que no veo que era generic y no el normal u.u para crear la lista

public class Balas_Pool : MonoBehaviour {

    [SerializeField]
    private Rigidbody bala;

    [SerializeField]
    private int tamaño_cargador;

    public List<Rigidbody> balas;


    private static Balas_Pool balas_pool;

    public static Balas_Pool Balas_pool
    {
        get
        {
            return balas_pool;
        }
    }

    void Awake()
    {
        if (balas_pool == null)
        {
            balas_pool = this;

            Preparar();
        }else
        {
            Destroy(gameObject);
        }
    } 
    
  private void CrearBalas()
    {
        Rigidbody crear = Instantiate(bala);
        crear.gameObject.SetActive(false);
        balas.Add(crear);


    }
    private void Preparar()
    {
        balas = new List<Rigidbody>();
        for (int i=0;i<tamaño_cargador;i++)
        {
            CrearBalas();
        }
       
    }

   public Rigidbody ObtenerBalas()
    {
        if (balas.Count == 0)
        {
            CrearBalas();
        }
        return CantidadBalas();
    }
     
    private Rigidbody CantidadBalas()
    {
        Rigidbody bala = balas[balas.Count - 1];
        balas.RemoveAt(balas.Count - 1);
        
        bala.gameObject.SetActive(true);
       
        return bala;
    }
    public void DesactivarBala(Rigidbody bala)
    {
        bala.gameObject.SetActive(false);
        balas.Add(bala);
    }



}
