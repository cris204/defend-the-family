﻿using UnityEngine;
using System.Collections;

public class Enemigo_factory : MonoBehaviour {

    [SerializeField]
    private GameObject enemigo;

    public static int valorRandom;

    public void AplicarCambios(GameObject  enemy)
    {
        valorRandom=Random.Range(1, 6);
        enemigo.GetComponent<Enemigos>().vida = valorRandom;
      
        Renderer enemigocolor =enemy. GetComponent<Renderer>();

        enemigocolor.material.color = ObtenerColor();

    }

    private Color ObtenerColor()
    {
        float r=Random.Range(0f,1f);
        float g = Random.Range(0f, 1f);
        float b = Random.Range(0f, 1f);
        Color color = new Color(r, g, b, 1f);
        return color;
    }


}
