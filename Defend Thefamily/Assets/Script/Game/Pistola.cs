﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Pistola : MonoBehaviour {
      
        
    public float fuerza;

    public int disparos;

  
    public int disparosTotales;

    [SerializeField]
    private Rigidbody rb;

    
    public GameObject recargar;

    void Awake()
    {
        GameController.municion = disparosTotales;
    }

    void OnEnable()
    {
        recargar.SetActive(false);

        GameController.municion_total_pistola = disparosTotales;

        GameController.municion = disparosTotales - disparos;
    }

  public virtual  void Start()
    {
        recargar.SetActive(false);
        
    }

  public virtual  void Update () {
      
        if (Input.GetButtonDown("Fire1") && disparos<disparosTotales)
        {
            disparos++;
            GameController.municion =disparosTotales-disparos;
            Shoot();

        }

        if (disparos == disparosTotales)
        {
            RecargarAviso();
            if (Input.GetButtonDown("Fire2"))//Tenia Recargar pero no se porque cuando desscargo el repo no se asigna en unity
            {
                recargar.SetActive(false);
                disparos = 0;
                GameController.municion = disparosTotales;
            }

        }

     

    }

    public virtual void Shoot()
    {
        Rigidbody bala = Balas_Pool.Balas_pool.ObtenerBalas();
        bala.transform.position = transform.position;
        bala.velocity=(transform.forward * fuerza);
       
            }

    public virtual void RecargarAviso()
    {
        recargar.SetActive(true);
       
    }


}
