﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;

public class RespawnEnemigos : MonoBehaviour {

    [SerializeField]
    private Enemigo_factory factory;
   
    [SerializeField]
    private GameObject enemigo;

    [SerializeField]
    private Transform[] respawns = new Transform[4];

    [SerializeField]
    private int cantidad_Enemigos = 10;

    public List<GameObject> enemigos;

    [SerializeField]
    private float tiempo=0f ;

    [SerializeField]
    private float tiempo_Respawn=5f;

    private int posicion;



    private static RespawnEnemigos enemigos_pool;

    public static RespawnEnemigos Enemigos_pool
    {
        get
        {
            return enemigos_pool;
        }
    }

    void Awake()
    {
        if (enemigos_pool == null)
        {
            enemigos_pool = this;
            PrepararEnemigos();
        }
        else
        {
            Destroy(gameObject);
        }
       
    }

	void Update () {

        tiempo = tiempo + 1 * Time.deltaTime;



        if (tiempo > tiempo_Respawn && !GameController.desactivar)
        {
            ObtenerEnemigos();
          
            tiempo = 0f;
                 
        }
        
	}

   private void CrearEnemigos()
    {
        GameObject enemy = Instantiate(enemigo);
        enemy.SetActive(false);

        GetComponent<Enemigo_factory>().AplicarCambios(enemy);

        enemigos.Add(enemy);
    }

   private void PrepararEnemigos()
    {
        enemigos = new List<GameObject>();

        for(int i = 0; i < cantidad_Enemigos; i++)
        {
            CrearEnemigos();
        }
    }

       public GameObject ObtenerEnemigos()
    {
        if (enemigos.Count == 0)
        {
            CrearEnemigos();
        }
        return CantidadEnemigos();
    }

    private GameObject CantidadEnemigos()
    {
        posicion = Random.Range(0, 4);
        GameObject enemigo = enemigos[enemigos.Count - 1];
        enemigos.RemoveAt(enemigos.Count - 1);
        enemigo.transform.position = respawns[posicion].position;
        enemigo.SetActive(true);
     
        return enemigo;
    }

    public void DesactivarEnemigo(GameObject enemigo)
    {

        enemigo.SetActive(false);
        enemigos.Add(enemigo);
                
    }

    public void DesactivarTodoEnemigo()
    {
        while (GameObject.FindGameObjectsWithTag("enemigo").Length > 0)
        {
            enemigos.Add(GameObject.FindGameObjectWithTag("enemigo"));
            GameObject.FindGameObjectWithTag("enemigo").SetActive(false);
          
        }
    }
}
