﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour {

    public static int vida;
    public static bool desactivar;
    private bool pistola;
    private bool pausa;
    public static int municion;
    public float alpha;


    public static int municion_total_pistola;

    public static int municion_total_shotGun;

    public Text vidas;

    public Text balas;

    [SerializeField]
    private Image blood;

    [SerializeField]
    private GameObject arma;

    public GameObject reiniciar;
    public GameObject menu;

    // Use this for initialization
    void Start () {
        alpha = 0;
        vida = 5;
        desactivar = false;
        pistola = true;
       
        arma.GetComponent<ShotGun>().enabled = false;
        arma.GetComponent<Pistola>().enabled = true;
        reiniciar.SetActive(false);
        menu.SetActive(false);
        Time.timeScale = 1;
        pausa = false;
    }


	
	// Update is called once per frame
	void Update () {
        blood.color = new Color(124f, 0, 0, alpha);
        vidas.text = "Familiares  " + vida.ToString();


        if (Input.GetButtonDown("CambiarArma") && !pausa)
        {
            CambiarArma();
        }

        if (Input.GetButtonDown("Pausa"))
        {
            Pausa();
        }

        if (pistola)
        {
            balas.text = municion.ToString() + " De " + municion_total_pistola;
        }
        else
        {
            balas.text = municion.ToString() + "  De " + municion_total_shotGun;
        }

        if (GameController.vida <= 0)
        {
            VerificarReinicio();
        }
	}

    private void VerificarReinicio()
    {
            
            Time.timeScale = 0;
            reiniciar.SetActive(true);
            menu.SetActive(true);
            arma.GetComponent<Pistola>().enabled = false;
            arma.GetComponent<ShotGun>().enabled = false;
          

            if (!desactivar)
            {
                RespawnEnemigos.Enemigos_pool.DesactivarTodoEnemigo();
            desactivar = true;
      
            }
        }

    private void CambiarArma()
    {
        if (pistola)
        {
            pistola = false;
        }
        else
        {
            pistola = true;
        }

        arma.GetComponent<Pistola>().enabled = pistola;
        arma.GetComponent<ShotGun>().enabled = !pistola;
        
    
    }
    public void Reiniciar()
    {
        arma.GetComponent<Pistola>().enabled = true;
        SceneManager.LoadScene("Juego");
        GetComponent<RespawnEnemigos>().enabled = true;
        reiniciar.SetActive(false);
        desactivar = false;
    }

    public void Menu()
    {
        SceneManager.LoadScene("Menu");
        desactivar = false;
    }

    void Pausa()
    {
        if (!pausa)
        {
            Time.timeScale = 0;
            arma.GetComponent<Pistola>().enabled = false;
            arma.GetComponent<ShotGun>().enabled = false;
            reiniciar.SetActive(true);
            menu.SetActive(true);
            pausa = true;
        }
        else
        {
            Time.timeScale = 1;
            arma.GetComponent<Pistola>().enabled = pistola;
            arma.GetComponent<ShotGun>().enabled = !pistola;
            reiniciar.SetActive(false);
            menu.SetActive(false);
            pausa = false;
        }
    }


}
