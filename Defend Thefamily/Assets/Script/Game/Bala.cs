﻿using UnityEngine;
using System.Collections;


[RequireComponent (typeof(Rigidbody))]
public class Bala : MonoBehaviour {

    [SerializeField]
    private Rigidbody rb;

    [SerializeField]
    private float tiempo =0;


    [SerializeField]
    private float tiempoDestruccion = 3f;


    void Awake()
    {
        rb = GetComponent<Rigidbody>();
    }

    void Update()
    {
        tiempo = tiempo + 1 * Time.deltaTime;

        if (tiempo >= tiempoDestruccion)
        {
            Balas_Pool.Balas_pool.DesactivarBala(rb);
            tiempo = 0f;
        }
    }

    void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.tag == "enemigo")
        {
            Balas_Pool.Balas_pool.DesactivarBala(rb);
        }

      
    }

}
