﻿using UnityEngine;
using System.Collections;

public class Enemigos : MonoBehaviour {

    
    public int vida ;
    private int vida_creacion;
    private GameObject objetivo;

    private Vector3 objetivo_posicion;

    private NavMeshAgent agente;

    void Awake()
    {
      
        vida_creacion = vida;
        objetivo = GameObject.Find("Main Camera");
        objetivo_posicion = objetivo.transform.position;
        agente = GetComponent<NavMeshAgent>();
    }

    void OnEnable()
    {

        vida = vida_creacion;

    }
	void Update () {

        agente.SetDestination(new Vector3 (objetivo_posicion.x,1,objetivo_posicion.z));
        
	}
    void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.tag == "bala")
        {
            vida--;
            if (vida <= 0)
            {
               
                RespawnEnemigos.Enemigos_pool.DesactivarEnemigo( this.gameObject);

            }
        }


    }
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "jugador")
        {
            RespawnEnemigos.Enemigos_pool.DesactivarTodoEnemigo();
            GameObject.Find("GameController").GetComponent<GameController>().alpha += 0.2f;
            GameController.vida--;
        }
    }

}
